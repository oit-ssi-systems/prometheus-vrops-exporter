#!/usr/bin/env python3

import hvac
import logging
import os
import asyncio
import nagini
import time

# No good certs in this world 😢
import requests
from prometheus_client import start_http_server, Gauge


INTERESTING_STATS = {
    "summary_oversized": ["summary_oversized_memory", "summary_oversized_vcpus"],
    "summary_undersized": ["summary_undersized_memory", "summary_undersized_vcpus"],
}


async def main(loop):
    requests.packages.urllib3.disable_warnings()

    # Vault connection with local token, or k8s auth
    vault = hvac.Client(url=os.environ.get("VAULT_ADDR"))
    if "VAULT_TOKEN" in os.environ:
        logging.debug("Using VAULT_TOKEN auth\n")
        vault.token = os.environ["VAULT_TOKEN"]
    elif "VAULT_OKD" in os.environ:
        logging.debug("Using K8s auth")
        f = open("/var/run/secrets/kubernetes.io/serviceaccount/token")
        jwt = f.read()
        vault.auth_kubernetes(
            "prometheus-vrops", jwt, mount_point=os.environ.get("VAULT_OKD")
        )

    summary_oversized_memory = Gauge(
        "summary_oversized_memory", "Oversized memory in kB", ["hostname"]
    )
    summary_oversized_vcpus = Gauge(
        "summary_oversized_vcpus", "Oversized virutal CPUs", ["hostname"]
    )
    summary_undersized_memory = Gauge(
        "summary_undersized_memory", "Undersized memory in kB", ["hostname"]
    )
    summary_undersized_vcpus = Gauge(
        "summary_undersized_vcpus", "Undersized virutal CPUs", ["hostname"]
    )

    start_http_server(8080)

    vrops_auth = vault.read("secret/shared_service_accounts/vrops")["data"]
    vcops = nagini.Nagini(
        host=vrops_auth["host"],
        verify=False,
        user_pass=(vrops_auth["username"], vrops_auth["password"]),
    )
    resources = vcops.get_resources()["resourceList"]
    resource_map = {}
    for resource in resources:
        resource_map[resource["identifier"]] = resource["resourceKey"]["name"]
    for resource in resources:
        for stats in vcops.get_latest_stats(id=resource["identifier"])["values"]:
            resource_name = resource_map.get(stats["resourceId"])
            all_metrics = {}
            for stat in stats["stat-list"]["stat"]:
                metric = (
                    stat["statKey"]["key"].lower().replace("|", "_").replace(" ", "_")
                )

                # Some stats have no data now
                if "data" not in stat:
                    continue

                value = int(stat["data"][0])
                all_metrics[metric] = value

            for interesting_stat, sub_stats in INTERESTING_STATS.items():
                if interesting_stat in all_metrics:
                    print(interesting_stat, sub_stats)
                    for sub_stat in sub_stats:
                        if all_metrics[sub_stat] == 0:
                            continue

                        print(resource_name, sub_stat, all_metrics[sub_stat])

                        if all_metrics["summary_oversized_memory"] > 0:
                            summary_oversized_memory.labels(resource_name).set(
                                all_metrics["summary_oversized_memory"]
                            )
                        if all_metrics["summary_oversized_vcpus"] > 0:
                            summary_oversized_vcpus.labels(resource_name).set(
                                all_metrics["summary_oversized_vcpus"]
                            )
                        if all_metrics["summary_undersized_memory"] > 0:
                            summary_undersized_memory.labels(resource_name).set(
                                all_metrics["summary_undersized_memory"]
                            )
                        if all_metrics["summary_undersized_vcpus"] > 0:
                            summary_undersized_vcpus.labels(resource_name).set(
                                all_metrics["summary_undersized_vcpus"]
                            )

    #               if metric in INTERESTING_STATS:
    #                   print(resource_name, metric, value)

    print("Starting up that sleep yo'")
    time.sleep(600)
    return 0


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
